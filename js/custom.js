console.log(getAllUrlParams(window.location.href));

function sendNewMember() {
  console.log('Sending data to sendToLeadManager...');
  $.ajax({
    type: 'POST',
    url: 'curl.php?' + getAllUrlParams(window.location.href),
    data: $('#newMember').serialize()
  })
    .done(function (data) {
      // show the response
      console.log(data);
      //if success show thank you message instead the form
      if (data == '204') {
        $('.popup').fadeIn();
        observer.disconnect();
        $('.popup').focus();
        //console.log('unfocus');
      }
    })
    .fail(function () {

      // just in case posting your form failed
      console.log("Sending data to sendToLeadManager failed.");

    });
  return false; // to prevent refreshing the whole page page
}

// New Member Validation
$("#newMember").pluginValidateForm({
  // showAllErrorsInOneSelector: true,
  // selectorToHideIfNoErrors: '.popup',
  // showErrorsSelector: '.errors',
  showErrorsAfterField: true,
  // showErrorsAfterField: false,
  addClassOnError: 'error',
  errorWrapper: {tag: 'div', class: 'error-text'},
  fields: ["name", "phone", "mail"],
  onSuccess: sendNewMember
});

// Focus on error fields:
var targetNode = document.getElementById('newMember');
var config = {attributes: true, childList: true, subtree: true};
// Callback function to execute when mutations are observed
var callback = function (mutationsList, observer) {
  for (var mutation of mutationsList) {
    if (mutation.type == 'attributes') {
      if (mutation.attributeName == 'class') {
        //console.log(mutation);
        if (mutation.target.classList.contains('error')) {
          mutation.target.focus();
          break;
        }
      }
    }
  }
};
// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);
// Start observing the target node for configured mutations
observer.observe(targetNode, config);
// Later, you can stop observing
//observer.disconnect();


// Limit characters for all inputs that have maxlength
$('input').keypress(function (e) {
  var input = $(e.target),
    maxLength = parseInt(input.attr('maxlength')),
    charCode = e.which || e.keyCode;
  var specials = [8, 9, 13, 16, 17, 18, 46]; // Tab, etc. ,19,20,27,33,34,35,36,37,38,39,40,45,46
  var char = String.fromCharCode(e.charCode);
  if (input.attr('type') == 'number') {
    var inputLength = input.val().toString().length;
  } else {
    var inputLength = input.val().length;
  }
  if (maxLength > 0 && ($.inArray(charCode, specials) == -1) && !(inputLength < maxLength)) { // stop on next char after maxlength
    //console.log('stopMAX');
    e.preventDefault();
  }
  if ((input.attr('data-pattern') == 'NumericChars') &&
    ($.inArray(charCode, specials) == -1) &&
    !(charCode >= 48 && charCode <= 57)) {
    //console.log('stopNAN');
    e.preventDefault();
  }
  //Forbidden numbers
  if (input.attr('data-pattern') == 'HebrewChars' && /[0-9]/.test(char)) {
    e.preventDefault();
  }
});

// Parse GET params
function getAllUrlParams(url) {
  // get query string from url (optional) or window
  //var queryString = url.split('?')[1]
  var queryString = url.replace(url.split('?')[0] + '?', "");
  //console.log(url.split('?')[0]);
  //console.log(queryString);
  // we'll store the parameters here
  var obj = {};
  // if query string exists
  if (queryString) {
    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];
  }
  return queryString;
}

// Accessibility plugin settings
accessibility_rtl = true;
pixel_from_side = 20;
pixel_from_start = 12;
css_style = 2;

$(document).ready(function () {
  $('#fiction').detach();
  $('.btn_accessibility').attr('tabindex', '2');
  $('.btn_accessibility').detach().appendTo('.accessibility');
  setTimeout(function () {
    $('.btn_accessibility').detach().appendTo('.accessibility');
  }, 5000)
});
