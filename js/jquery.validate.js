/*
 *  jquery-boilerplate - v4.0.0
 *  A jump-start for jQuery plugins development.
 *  http://jqueryboilerplate.com
 *
 *  Made by Zeno Rocha
 *  Under MIT License
 */
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {

  "use strict";

  // undefined is used here as the undefined global variable in ECMAScript 3 is
  // mutable (ie. it can be changed by someone else). undefined isn't really being
  // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
  // can no longer be modified.

  // window and document are passed through as local variable rather than global
  // as this (slightly) quickens the resolution process and can be more efficiently
  // minified (especially when both are regularly referenced in your plugin).

  // Create the defaults once
  var pluginValidateForm = "pluginValidateForm",
    defaults = {
      errorArray: [],
      showErrorsSelector: '',
      showErrorsAfterField: true,
      showAllErrorsInOneSelector: false,
      selectorToHideIfNoErrors: '',
      addClassOnError: [],
      HebrewChars: new RegExp("^[\u0590-\u05FF\- \u0027\u0022\u2010\u2212\u00AD\u002D\uFF0D]+$"), // allows space \u2010 \u2212 \u002D \u00AD \uFF0D
      NumericChars: new RegExp("^[0-9\-]+$"),
      EmailChars: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
      onSuccess: function(){}
    };
    // TODO: вынести в отдельный объект HebrewChars, errorArray, NumericChars, EmailChars и передать его в settings, так чтобы юзер не мог менять эти данные при вызове плагина

  // The actual plugin constructor
  function Plugin(element, options) {
    this.element = element;

    // jQuery has an extend method which merges the contents of two or
    // more objects, storing the result in the first object. The first object
    // is generally empty as we don't want to alter the default options for
    // future instances of the plugin
    this.settings = $.extend({}, defaults, options);
    this._defaults = defaults;
    this._name = pluginValidateForm;
    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {
    init: function () {
      // Place initialization logic here
      // You already have access to the DOM element and
      // the options via the instance, e.g. this.element
      // and this.settings
      // you can add more functions like the one below and
      // call them like the example bellow
      $(this.settings.selectorToHideIfNoErrors).hide();
      this.bindSubmitEvent();
    },

    bindSubmitEvent: function () {
      // some logic on plugin init
      $(this.element).submit(this.validateFields.bind(this));
    },

    collectErrors: function (field, errorText) {
      //field.append('<div class="error">' + errorText + '</div>');
      this.settings.errorArray.push({field: field, errorText: errorText});
      //console.log(this.settings.errorArray);
    },
    showErrors: function () {
      var _this = this;
      if (this.settings.errorArray.length > 0 && $(this.settings.selectorToHideIfNoErrors).length) {
        $(this.settings.selectorToHideIfNoErrors).show();
      }
      this.settings.errorArray.forEach(function (errorArrayItem) {
        if (_this.settings.showErrorsAfterField == true) {
          errorArrayItem.field.after(_this.wrapErrorText(errorArrayItem.errorText));
        }
        //if you need to show all errors in one selector
        if (_this.settings.showAllErrorsInOneSelector == true) {
          $(_this.settings.showErrorsSelector).append(_this.wrapErrorText(errorArrayItem.errorText));
        }
        errorArrayItem.field.addClass(_this.settings.addClassOnError);
        //console.log($(_this.settings.showErrorsSelector));
      });
    },
    wrapErrorText: function (errorText) {
      return ('<' + this.settings.errorWrapper.tag + ' class="' + this.settings.errorWrapper.class + '">' + errorText + '</' + this.settings.errorWrapper.tag + '>');
    },
    clearOldErrors: function () {
      // remove error messages
      $(this.settings.errorWrapper.tag + '.' + this.settings.errorWrapper.class).detach();
      this.settings.errorArray = [];
      //Hide the popup @selectorToHideIfNoErrors@
      $(this.settings.selectorToHideIfNoErrors).hide();
      // remove class @addClassOnError@ from input with error
      //console.log($('.' + this.settings.addClassOnError).length);
      var _this = this;
      $('.' + _this.settings.addClassOnError).each(function () {
        $(this).removeClass(_this.settings.addClassOnError);
      });
    },
    validateFields: function (e) {
      var _this = this;
      e.preventDefault();
      this.clearOldErrors();
      this.settings.fields.forEach(function (fieldName) {
        let field = $('[name=' + fieldName + ']');
        let fieldValLength = field.val().length;
        if (field.attr('type') == 'number') {
          fieldValLength = field.val().toString().length;
        }
        if (
          (field.attr('required') && field.attr('type') == 'checkbox' && !field.is(':checked')) // if field has required attribute, but is empty
          || // or
          (field.attr('required') && field.attr('type') != 'checkbox' && !field.val()) // if field is checkbox has required attribute, but is empty
        ) {
          _this.collectErrors(field, field.attr('data-error'));
        } else if (field.attr('minlength') && (fieldValLength < field.attr('minlength')) && (fieldValLength != 0)) {
          // if field has minlength attribute but the value has less characters
          // if field has special error for min length (data-minerror) show it instead common error
          if (typeof field.attr('data-minerror') === "undefined") {
            _this.collectErrors(field, field.attr('data-error'));
          } else {
            _this.collectErrors(field, field.attr('data-minerror'));
          }
          //console.log('minlength', _this.collectErrors(field, field.attr('data-minerror') || _this.collectErrors(field, field.attr('data-error'))));
        } else if (field.attr('maxlength') && (fieldValLength > field.attr('maxlength')) && (fieldValLength != 0)) {
          // if field has maxlength attribute but the value has less characters
          // if field has special error for min length (data-maxerror) show it instead common error
          if (typeof field.attr('data-maxerror') === "undefined") {
            _this.collectErrors(field, field.attr('data-error'));
          } else {
            _this.collectErrors(field, field.attr('data-maxerror'));
          }
        } else if (field.attr('data-pattern') == 'HebrewChars' && !(_this.settings.HebrewChars.test(field.val())) && (field.val() !== '')) {
          // if pattern is required but value doesn't fit
          if (typeof field.attr('data-patternerror') === "undefined") {
            _this.collectErrors(field, field.attr('data-error'));
          } else {
            _this.collectErrors(field, field.attr('data-patternerror'));
          }
        } else if (field.attr('data-pattern') == 'EmailChars' && !(_this.settings.EmailChars.test(field.val())) && (field.val() !== '')) {
          // if pattern is required but value doesn't fit
          if (typeof field.attr('data-patternerror') === "undefined") {
            _this.collectErrors(field, field.attr('data-error'));
          } else {
            _this.collectErrors(field, field.attr('data-patternerror'));
          }
        } else if (field.attr('data-pattern') == 'NumericChars' && !(_this.settings.NumericChars.test(field.val())) && (field.val() !== '')) {
          // if pattern is required but value doesn't fit
          if (typeof field.attr('data-patternerror') === "undefined") {
            _this.collectErrors(field, field.attr('data-error'));
          } else {
            _this.collectErrors(field, field.attr('data-patternerror'));
          }
        }
        /*else {
          console.log('success field');
        }*/
      });

      // console.log('errors', this.settings.errorArray.length);
      if(this.settings.errorArray.length == 0){
        this.settings.onSuccess();
      }

      this.showErrors();
    }

  });

  // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations
  $.fn[pluginValidateForm] = function (options) {
    return this.each(function () {
      if (!$.data(this, "plugin_" + pluginValidateForm)) {
        $.data(this, "plugin_" +
          pluginValidateForm, new Plugin(this, options));
      }
    });
  };

})(jQuery, window, document);