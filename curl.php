<?php


// New lead creation (API REQUEST #1)

$ch = curl_init("https://api.leadmanager.co.il/odata/Leads");
$data = array("ChannelID" => "36497"); //TODO: change
$data_string = json_encode($data);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

// Configuring headers
$headers = array(
    'lm-hdr-acc: 1619',
    'lm-hdr-key: cbc0e8c67f6b457c91449321420c5494',
    'content-type: application/json',
);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


// Receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);
$server_output_array = json_decode($server_output, true);
//var_dump($server_output_array);
$lead_id = $server_output_array['ID'];
curl_close($ch);


// Changing the lead (API REQUEST #2)
$ch = curl_init("https://api.leadmanager.co.il/odata/Leads(" . $lead_id . ")");

/* RAW JSON EXAMPLE
$data_string = '{
   "ChannelID":"35608",
   "Fields":[
      {
         "ID":"40486",
         "Value":"דביר בדיקה"
      },
      {
         "ID":"40487",
         "Value":"0547401852"
      },
      {
         "ID":"40488",
         "Value":"test@test.com"
      },
      {
         "ID":"40746",
         "Value":"fld_40746"
      }
   ]
}';
*/

//var_dump($_POST);


$data = array(
    "ChannelID" => "35608",
    "Fields" => array()
);

//var_dump($_GET);
// extend the exicting array with values of type: fld_40747=607615567
foreach ($_GET as $key => $value) {
    if (substr($key, 0, 3) == 'fld') {
        //remove 'fld' from the beginning of key
        if (substr($key, 4) !== "40486" && substr($key, 4) !== "40487" && substr($key, 4) !== "40488")
            array_push($data['Fields'], array("ID" => substr($key, 4), "Value" => $value));
    }
}

// Set name
array_push($data['Fields'], array(
    "ID" => "40486",
    "Value" => $_POST["name"]
));

// Set phone
array_push($data['Fields'], array(
    "ID" => "40487",
    "Value" => $_POST["phone"]
));

// Set mail
array_push($data['Fields'], array(
    "ID" => "40488",
    "Value" => $_POST["mail"]
));

$data_string = json_encode($data);

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
//var_dump($data_string);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

// Configuring curl options
$headers = array(
    'content-Type: application/json',
    'lm-hdr-acc: 1619',
    'lm-hdr-key: cbc0e8c67f6b457c91449321420c5494',
);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

// Further processing ...
// should have 204 code if successful
echo curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close($ch);

?>
