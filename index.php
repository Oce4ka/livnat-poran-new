<?php header('X-Frame-Options: DENY'); ?>
<!doctype html>
<html lang="he" dir="rtl">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:title" content="לבנת פורן- החמרת תקנות"/>
  <meta http-equiv="Access-Control-Allow-Origin" content="http://www.example.org">
  <title>לבנת פורן- החמרת תקנות</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/responsive.css">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
  <link rel="manifest" href="site.webmanifest">
</head>
<body class="framed">

<script type="text/javascript">
  if (top == self)
    document.body.className = 'notframed';
</script>
<div id="warning">
  This site cannot be used in a frame or with JavaScript disabled.
</div>
<div id="orientation-block"></div>

<div class="header">
  <div class="container">
    <div tabindex="5" class="logo" alt="לוגו חברה"></div>
    <div tabindex="5" class="woman"></div>
    <div class="accessibility">
      <div id="fiction" class="btn_accessibility"></div>
    </div>
  </div>
</div>

<div class="content">
  <div class="container">
    <div tabindex="10" class="section-image" alt="מלאו פרטים ונציג יחזור אליך">&nbsp;</div>
    <div class="section-form">
      <div class="headings">
        <h1 tabindex="10">חולה סיעודי,</h1>
        <h2 tabindex="10">קשה להתמודד לבד<br>מול חברת הביטוח</h2>
        <hr>
        <h3 tabindex="10">
          ייתכן ומגיעה לך קצבה מחברת הביטוח<br>
          ואנחנו כאן כדי לסייע לך כפי שסייענו<br>
          לאלפי ישראלים במצבך.
        </h3>
        <h4 tabindex="10">פנה אלינו בהקדם!</h4>
      </div>
      <form class="form" id="newMember" action="/" novalidate>
        <div class="popup">
          <div tabindex="10" class="popup-content">
            <div tabindex="10" class="popup-title">תודה!</div>
            <div tabindex="10" class="popup-text">נציג שלנו יחזור אליכם<br> לפרטים נוספים</div>
          </div>
        </div>

        <h5 tabindex="10">אל תוותר על הכסף המגיע לך!</h5>
        <h6 class="desktop-only" tabindex="10">
          חייג אלינו
          <a tabindex="10" class="phone" href="tel:037207589">03-7207589</a>
          או מלא פרטיך:
        </h6>
        <h6 class="mobile-only" tabindex="10">
          <a tabindex="10" class="phone" href="tel:037207589">חייג אלינו</a>
          או מלא פרטיך:
        </h6>

          <?php $ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']); ?>
        <input id="ip" type="hidden" name="ip" value="<?php echo $ip; ?>">

        <div class="field">
          <!--name-->
          <input tabindex="10" name="name" type="text" placeholder="שם מלא:" required maxlength="100"
                 minlength="2" data-pattern="HebrewChars"
                 data-error="שם מלא הינו חובה">
        </div>
        <div class="field">
          <!--phone-->
          <input tabindex="10" name="phone" type="tel" placeholder="טלפון:" required minlength="10"
                 maxlength="10" data-pattern="NumericChars"
                 data-error="מספר טלפון לא תקין">
        </div>
        <div class="field">
          <!--mail-->
          <input tabindex="10" name="mail" type="email" placeholder=":דוא”ל:" maxlength="60"
                 data-pattern="EmailChars" data-error='כתובת דוא"ל לא תקינה'
                 data-error-new='כתובת הדוא"ל שהוזנה אינה מזוהה במאגר חברי המועדון. אנא בדקו ונסו שנית'>
        </div>

        <p class="agreement">
          לחיצה על ״שלח״ מהווה הסכמה
          <a tabindex="10" target="_blank"
             alt="לחצו כאן לצפיה במדיניות הפרטיות. מסמך מדיניות הפרטיות יפתח כPDF בטאב חדש בדפדפן"
             href="https://www.medical-rights.co.il/%D7%90%D7%95%D7%93%D7%95%D7%AA/%D7%A0%D7%A2%D7%99%D7%9D-%D7%9C%D7%94%D7%9B%D7%99%D7%A8/%D7%9E%D7%93%D7%99%D7%A0%D7%99%D7%95%D7%AA-%D7%94%D7%A4%D7%A8%D7%98%D7%99%D7%95%D7%AA-%D7%A9%D7%9C-%D7%94%D7%90%D7%AA%D7%A8">למדיניות
            הפרטיות</a>
          של החברה.
        </p>
        <div class="field">
          <button tabindex="10"
                  alt="לחצו כאן לבדיקת שדות ההזנה ולהשארת מספר טלפון וכתובת דואר אלקטרוני לטובת שיחה עם נציג"
                  type="submit">
            שלח
            <img src="img/hearts.png" alt="icon">
          </button>
        </div>
        <p tabindex="10" class="note">*השירות אינו משפטי.</p>
      </form>

    </div>
  </div>
</div>

<!-- scripts -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/accessibility_pro.min.js" type="text/javascript"></script>
<script src="js/custom.js"></script>

</body>
</html>